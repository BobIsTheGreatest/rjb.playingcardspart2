#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank : int{
	//ONE=1,			//One is the same as Ace
	TWO=2,
	THREE=3,
	FOUR=4,
	FIVE=5,
	SIX=6,
	SEVEN=7,
	EIGHT=8,
	NINE=9,
	TEN=10,
	JACK=11,
	QUEEN=12,
	KING=13,
	ACE=14
};

enum Suit {
	CLUB,
	SPADE,
	HEART,
	DIAMOND
};

struct Card 
{
	Suit face;
	Rank value;
	string name;
	int deckTotal = 52;
};

std::string cardSuittoString(Suit);
std::string cardValuetoString(Rank);
void printCard(Card);
Card HighCard(Card, Card);

int main()
{
	Card c1;
	c1.face = CLUB;
	c1.value = QUEEN;


	Card c2;
	c2.face = SPADE;
	c2.value = THREE;

	Card c3 = HighCard(c1, c2);

	cout << "Card 1 is the: ";
	
	printCard(c1);

	cout << "Card 2 is the: ";
	printCard(c2);
	
	cout << "The higher card is: ";
	printCard(c3);

	cout << "Press any key to exit";

	_getch();

	return 0;
}

/*
	Prints out the rank and suit of the card. Ex: The Queen of Diamonds
*/
void printCard(Card cardToPrint)
{
	cout << cardValuetoString(cardToPrint.value) << " of " << cardSuittoString(cardToPrint.face) << "\n \n";
}

/*
	Determines which of the two supplied cards has the higher rank.
*/
Card HighCard(Card card1, Card card2)
{
	Card highCard = card2;
	if (card1.value > card2.value)
	{
		highCard = card1;
	}
	
	return highCard;
}

string cardValuetoString(Rank rank) {

	std::string valueString;

	switch (rank) {

	case TWO:
		valueString = "2";
		break;
	case THREE:
		valueString = "3";
		break;
	case FOUR:
		valueString = "4";
		break;
	case FIVE:
		valueString = "5";
		break;
	case SIX:
		valueString = "6";
		break;
	case SEVEN:
		valueString = "7";
		break;
	case EIGHT:
		valueString = "8";
		break;
	case NINE:
		valueString = "9";
		break;
	case TEN:
		valueString = "10";
		break;
	case JACK:
		valueString = "Jack";
		break;
	case QUEEN:
		valueString = "Queen";
		break;
	case KING:
		valueString = "King";
		break;
	case ACE:
		valueString = "Ace";
		break;
	default:
		valueString = "Invalid card";
		break;

	}
	return valueString;
}

string cardSuittoString(Suit type) 
{
	switch (type) {
	case CLUB:
		return "Clubs";
	case SPADE:
		return "Spades";
	case HEART:
		return "Hearts";
	case DIAMOND:
		return "Diamonds";
	default:
		return "Invalid card";
	}
}